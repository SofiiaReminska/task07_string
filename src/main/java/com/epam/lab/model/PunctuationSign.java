package com.epam.lab.model;

import java.util.Objects;
import java.util.regex.Pattern;

public class PunctuationSign extends SentencePart {
    public static final Pattern SPLIT_PUNCTUATION_REGEX = Pattern.compile("((?<=[.,:;\"'?!()])|(?=[.,:;\"'?!()]))");

    private String value;

    public PunctuationSign() {

    }

    public PunctuationSign(String sign) {
        this.value = sign;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static boolean isPunctuation(String s) {
        return Pattern.compile("[.,:;\"'?!()]").matcher(s).matches();
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PunctuationSign that = (PunctuationSign) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
