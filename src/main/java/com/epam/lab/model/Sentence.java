package com.epam.lab.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Sentence {
    public static final Pattern SPLIT_TO_SENTENCE_REGEX = Pattern.compile("(?<=[.?!])");
    private List<? super SentencePart> sentencePartList;

    public Sentence() {
        sentencePartList = new ArrayList<>();
    }

    public Sentence(List<? super SentencePart> sentencePartList) {
        this.sentencePartList = sentencePartList;
    }

    public List<? super SentencePart> getSentencePartList() {
        return sentencePartList;
    }

    public List<Word> getWords() {
        return getSentencePartList().stream().filter(p -> p instanceof Word).map(w -> ((Word) w)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return sentencePartList + " " + getLastSign();
    }

    public PunctuationSign getLastSign() {
        final Object o = sentencePartList.get(sentencePartList.size() - 1);
        if (o instanceof PunctuationSign)
            return (PunctuationSign) o;
        else throw new IllegalStateException("Cannot find last sign of sentence");
    }
}
