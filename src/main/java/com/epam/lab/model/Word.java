package com.epam.lab.model;

import java.util.Objects;
import java.util.regex.Pattern;

public class Word extends SentencePart implements Comparable<Word>{
    public static final Pattern SPLIT_TO_WORD_REGEX = Pattern.compile("\\s+");
    private String value;

    public Word() {

    }

    public Word(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return value.equalsIgnoreCase(((Word) o).value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public int compareTo(Word o) {
        return value.compareTo(o.value);
    }
}
