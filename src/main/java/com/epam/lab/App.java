package com.epam.lab;

import com.epam.lab.view.ViewLocale;

public class App {
    public static void main(String[] args) {
        new ViewLocale().displayMenu();
    }
}
