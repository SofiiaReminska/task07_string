package com.epam.lab.task;

import com.epam.lab.FileUtil;
import com.epam.lab.model.Sentence;
import com.epam.lab.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task1 {
    private static final Logger LOGGER = LogManager.getLogger(Task1.class);

    public static void main(String[] args) {
        maxNumberOfSentencesSameWords();
    }

    public static void maxNumberOfSentencesSameWords() {
        List<Sentence> sentences = FileUtil.getSentenceList();
        final Map<Word, Long> collect = sentences.stream().flatMap(s -> s.getWords().stream().distinct())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        final Entry<Word, Long> stringLongEntry = collect.entrySet()
                .stream().max(Comparator.comparing(Entry::getValue))
                .orElseThrow(IllegalStateException::new);
        LOGGER.info("Max number of sentences with same words: {}", stringLongEntry);
    }
}
