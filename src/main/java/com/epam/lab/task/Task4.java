package com.epam.lab.task;

import com.epam.lab.FileUtil;
import com.epam.lab.model.Sentence;
import com.epam.lab.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Task4 {

    private static final Logger LOGGER = LogManager.getLogger(Task4.class);

    public static void findQuestioningSentences() {
        Scanner sc = new Scanner(System.in);
        LOGGER.info("Enter word length:");
        int wordLength = sc.nextInt();
        List<Sentence> sentences = FileUtil.getSentenceList();
        final List<Word> words = sentences.stream()
                .filter(sentence -> sentence.getLastSign().getValue().equals("?"))
                .flatMap(qs -> qs.getWords().stream())
                .distinct()
                .filter(w -> w.getValue().length() == wordLength)
                .collect(Collectors.toList());
        LOGGER.info(words);
    }

    public static void main(String[] args) {
        findQuestioningSentences();
    }
}
