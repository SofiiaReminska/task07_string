package com.epam.lab.task;

import com.epam.lab.FileUtil;
import com.epam.lab.model.Sentence;
import com.epam.lab.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Task3 {
    private static final Logger LOGGER = LogManager.getLogger(Task3.class);

    public static void main(String[] args) {
        findUniqueWord();
    }

    public static void findUniqueWord() {
        List<Sentence> sentences = FileUtil.getSentenceList();
        List<Word> firstSentenceWords = sentences.get(0).getWords();
        sentences.remove(0);
        final List<Word> restOfWords = sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .collect(Collectors.toList());
        final Optional<Word> uniqueWord = firstSentenceWords.stream()
                .filter(fsw -> !restOfWords.contains(fsw))
                .findFirst();
        if (uniqueWord.isPresent()) {
            LOGGER.info("Unique word in first sentence is: {}", uniqueWord.get());
        } else {
            LOGGER.error("error");
        }
    }
}
