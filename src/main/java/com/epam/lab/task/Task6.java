package com.epam.lab.task;

import com.epam.lab.FileUtil;
import com.epam.lab.model.Sentence;
import com.epam.lab.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task6 {
    private static final Logger LOGGER = LogManager.getLogger(Task6.class);

    public static void main(String[] args) {
        sortWordsOfText();
    }

    public static void sortWordsOfText() {
        List<Sentence> sentences = FileUtil.getSentenceList();
        final List<Word> words = sentences.stream().flatMap(sentence -> sentence.getWords().stream()).distinct().sorted().collect(Collectors.toList());
        String tmp = "\0";
        String wordMessage = "";
        for (Word w : words) {
            if (!w.getValue().startsWith(tmp)) {
                wordMessage = "\t";
                tmp = String.valueOf(w.getValue().charAt(0));
            }
            wordMessage = wordMessage.concat(w.toString());
            LOGGER.info(wordMessage);
            wordMessage = "";
        }
    }
}
