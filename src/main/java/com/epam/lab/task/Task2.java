package com.epam.lab.task;

import com.epam.lab.FileUtil;
import com.epam.lab.model.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class Task2 {
    private static final Logger LOGGER = LogManager.getLogger(Task2.class);

    public static void main(String[] args) {
        sortSentencesByNumberOfWords();
    }

    public static void sortSentencesByNumberOfWords() {
        List<Sentence> sentences = FileUtil.getSentenceList();
        LOGGER.info("Splited sentences {}", sentences);
        final List<Sentence> collect = sentences.stream()
                .sorted(Comparator.comparingInt(s -> (int) s.getWords().size()))
                .collect(Collectors.toList());
        LOGGER.info("Sorted splited sentences by number of words in sentence {}", collect);
    }
}
