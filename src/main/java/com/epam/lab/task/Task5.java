package com.epam.lab.task;

import com.epam.lab.FileUtil;
import com.epam.lab.model.Sentence;
import com.epam.lab.model.SentencePart;
import com.epam.lab.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Task5 {
    private static final Logger LOGGER = LogManager.getLogger(Task5.class);

    public static void replaceWords() {
        List<Sentence> sentences = FileUtil.getSentenceList();
        List<Sentence> swappedSentences = new ArrayList<>();
        LOGGER.info(sentences);
        for (Sentence s : sentences) {
            final List<SentencePart> parts = (List<SentencePart>) s.getSentencePartList();
            final Optional<Word> vowelStartWord = s.getWords().stream().filter(w -> w.getValue().matches("^[aoeiuy].*$")).findFirst();
            final Optional<Word> maxLengthWord = s.getWords().stream().max(Comparator.comparingInt(w -> (int) w.getValue().length()));
            if (vowelStartWord.isPresent() && maxLengthWord.isPresent()) {
                Collections.swap(parts, parts.indexOf(vowelStartWord.get()), parts.indexOf(maxLengthWord.get()));
            } else {
                LOGGER.warn("Didn't swapped words in sentence");
            }
            swappedSentences.add(new Sentence(parts));
        }
        LOGGER.info(swappedSentences);
    }

    public static void main(String[] args) {
        replaceWords();
    }
}
