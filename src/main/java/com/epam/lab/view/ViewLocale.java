package com.epam.lab.view;

import com.epam.lab.task.Task1;
import com.epam.lab.task.Task2;
import com.epam.lab.task.Task3;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ViewLocale {
    private static final Logger LOGGER = LogManager.getLogger(ViewLocale.class);
    private Locale locale;
    private ResourceBundle bundle;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }

    public ViewLocale() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", () -> internationalizeMenu("pl"));
        methodsMenu.put("2", () -> internationalizeMenu("uk"));
        methodsMenu.put("3", () -> internationalizeMenu("en"));
        methodsMenu.put("4", Task1::maxNumberOfSentencesSameWords);
        methodsMenu.put("5", Task2::sortSentencesByNumberOfWords);
        methodsMenu.put("6", Task3::findUniqueWord);
    }

    private void internationalizeMenu(String language) {
        locale = new Locale(language);
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        displayMenu();
    }

    private void outputMenu() {
        LOGGER.info("MENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                LOGGER.info(menu.get(key));
            }
        }
    }

    public void displayMenu() {
        String keyMenu;
        do {
            outputMenu();
            LOGGER.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                LOGGER.error("Incorrect input");
            }
        } while (!keyMenu.equalsIgnoreCase("Q"));
    }
}
