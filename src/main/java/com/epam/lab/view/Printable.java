package com.epam.lab.view;

@FunctionalInterface
public interface Printable {
    void print();
}
