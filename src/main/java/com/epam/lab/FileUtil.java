package com.epam.lab;

import com.epam.lab.model.PunctuationSign;
import com.epam.lab.model.Sentence;
import com.epam.lab.model.SentencePart;
import com.epam.lab.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtil {

    private static final Logger LOGGER = LogManager.getLogger(FileUtil.class);

    public static String readDemoText() {
        File file = new File("src/main/resources/text.txt");
        StringBuilder textBuilder = new StringBuilder();
        try (Scanner sc = new Scanner(file)) {
            while (sc.hasNextLine()) {
                textBuilder.append(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found", e);
        }
        return textBuilder.toString();
    }

    public static List<Sentence> getSentenceList() {
        String text = readDemoText();
        text = text.replaceAll("[\t\n\r]+", " ");
        List<Sentence> sentences = new ArrayList<>();
        final List<String> sentenceStrings = Arrays.stream(text.split(Sentence.SPLIT_TO_SENTENCE_REGEX.pattern()))
                .collect(Collectors.toList());
        sentenceStrings.stream()
                .map(String::trim)
                .map(FileUtil::splitWordsAndPunctuation)
                .map(sentencePartStream -> sentencePartStream
                        .collect(Collectors.toList()))
                .forEach(words -> {
                    Sentence ss = new Sentence();
                    ss.getSentencePartList().addAll(words);
                    sentences.add(ss);
                });
        return sentences;
    }

    private static Stream<SentencePart> splitWordsAndPunctuation(String s) {
        return Arrays.stream(s.split(Word.SPLIT_TO_WORD_REGEX.pattern()))
                .flatMap(value -> {
                    return Arrays.stream(value.split(PunctuationSign.SPLIT_PUNCTUATION_REGEX.pattern()))
                            .map(w -> PunctuationSign.isPunctuation(w) ? new PunctuationSign(w) : new Word(w));
                });
    }
}
